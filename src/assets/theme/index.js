import { createMuiTheme } from '@material-ui/core/styles'
import { colors } from '../colors'
const theme = createMuiTheme({
  palette: {
    primary: {
      main: colors.green,
      light: colors.green_light,
      dark: colors.green_dark,
      contrastText: colors.white
    },
    secondary: {
      light: '#ff7961',
      main: '#f44336',
      dark: '#ba000d',
      contrastText: colors.black
    }
  },
  status: {
    danger: colors.red_danger
  },
  typography: {
    fontFamily: [
      '"HelveticaNeue"', 'Roboto', 'Arial', 'sans-serif'
    ]
  }
})

export default theme
