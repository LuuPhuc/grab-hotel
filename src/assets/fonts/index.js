export const fonts = {
  h1: 35,
  h2: 28,
  h3: 20,
  h4: 17,
  h5: 14,
  large: 16,
  small: 13,
  bold: 'bold',
  none: 'none'
}
