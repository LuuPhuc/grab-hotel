export const colors = {
  // White
  white: '#ffffff',
  white_gray: '#f8f8f8',
  white_gray_unable: '#dfdfe4',
  white_commnet_background: '#f2f2f2',
  // Gray
  gray: '#fafafa',

  // Black
  black: '#000000',
  black_gray: '#757575',
  black_gray_light: '#bfbfbf',
  black_normal_text: '#4a4a4a',

  // Primary Color
  green: '#169f84',
  green_light: '#29d7b4',
  green_dark: '#118c74',

  // Orange
  orange: '#f15b00',
  orange_light: '#fc9700',
  orange_super_light: '#fed16e',

  // Red
  red: '#eb4141',
  red_danger: '#f44336',

  // Pink
  pink_light: '#fff3f3',

  // Transparent
  transparent: 'transparent'
}
