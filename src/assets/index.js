import Colors from './colors'
import Fonts from './font'
import Images from './images'

export { Colors, Fonts, Images }
