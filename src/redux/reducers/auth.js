import { authActionTypes } from '../actions/auth'

const defaultState = {
  user: null,
  error: null
}

const authReducer = (state = defaultState, action) => {
  switch (action.type) {
    case authActionTypes.fetchInfo:
      return { ...state, user: null }
    case authActionTypes.fetchInfoSuccess:
      return { ...state, user: action.payload }
    case authActionTypes.fetchInfoFailed:
      return { ...state, user: null, error: action.error }
    default:
      return state
  }
}

export default authReducer
