export const authActionTypes = {
  login: 'LOGIN',
  regiser: 'REGISTRY',
  fetchInfo: 'FETCH_INFO',
  fetchInfoSuccess: 'FETCH_INFO_SUCCESS',
  fetchInfoFailed: 'FETCH_INFO_FAILED',
}

export const fetchInfoAction = (payload) => ({
  type: authActionTypes.fetchInfo,
  payload
})

export const fetchInfoFailed = (error) => ({
  type: authActionTypes.fetchInfoFailed,
  error
})