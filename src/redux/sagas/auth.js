import { all, takeEvery } from 'redux-saga/effects'
import { authActionTypes } from '../actions/auth'

export default function* authSaga() {
  yield all([
    takeEvery(authActionTypes.fetchInfo)
  ])
}
