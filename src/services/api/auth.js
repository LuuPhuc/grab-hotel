import request from '../../libs/request'

function loginApi (payload) {
  return request.post('/login', payload)
}

function register (payload) {
  return request.post('/', payload)
}