import React from 'react'
import { Header } from '../../components/layouts/Header'

function HomePage () {
  return (
    <div>
      <Header />
    </div>
  )
}

export default HomePage
