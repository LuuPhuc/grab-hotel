import React from 'react'
import ReactDOM from 'react-dom'
import './i18n'
import App from './App'
import store from './redux/store'
import theme from './assets/theme'
import { Provider } from 'react-redux'
import { ThemeProvider } from '@material-ui/core/styles'
import './index.scss'

ReactDOM.render(
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </Provider>,
  document.getElementById('root')
)
