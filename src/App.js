import React from 'react'
import { Route, Router, Switch } from 'react-router'
import { createBrowserHistory } from 'history'
import HomePage from './pages/HomePage'

function App () {
  const history = createBrowserHistory()
  return (
    <Router history={history}>
      <Switch>
        <Route path='/' component={HomePage} />
      </Switch>
    </Router>
  )
}

export default App
