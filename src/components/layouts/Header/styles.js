import { makeStyles } from '@material-ui/core/styles'

export const useHeaderStyles = makeStyles((theme) => ({
  background: {
      width: '100%',
      position: 'relative'
  }
}))
