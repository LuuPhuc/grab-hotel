import React from 'react'
import MenuAppBar from '../Navbar'
import SimpleTabs from '../../base/Tabs'
import { images } from '../../../assets/images'
import { useHeaderStyles } from './styles'

export const Header = () => {
  const classes = useHeaderStyles()
  return (
    <header className='header'>
      <MenuAppBar />
      <img src={images.main_background.url} className={classes.background} />
      <SimpleTabs />
    </header>
  )
}
