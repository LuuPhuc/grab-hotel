import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    '& .MuiToolbar-regular': {
      minHeight: 120,
      margin: '0 auto'
    },
    '& .MuiButton-root': {
      fontSize: '1rem'
    }
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1
  },
  backgroundSize: {
    width: '35%'
  }
}))
