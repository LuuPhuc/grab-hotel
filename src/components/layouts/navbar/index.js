import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Grid from '@material-ui/core/Grid'
import PositionedPopper from '../../base/Popper'
import { useStyles } from './styles'
import { images } from '../../../assets/images'
export default function MenuAppBar () {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <AppBar position='absolute' color='transparent' elevation={0}>
        <Toolbar>
          <Grid className={classes.grid}>
            <img src={images.main_logo.url} className={classes.backgroundSize} />
          </Grid>
          <PositionedPopper />
        </Toolbar>
      </AppBar>
    </div>
  )
}
