import { Button } from '@material-ui/core'
import withStyles from '@material-ui/core/styles/withStyles'
import { colors } from '../../../assets/colors'
import { fonts } from '../../../assets/fonts'
const OrangeButton = withStyles(theme => ({
  root: {
    textTransform: 'none',
    fontWeight: fonts.bold,
    color: colors.white,
    backgroundColor: colors.orange,
    padding: '10px 40px',
    borderRadius: '30px',
    marginLeft: theme.spacing(2)
  }
}))(Button)

export default OrangeButton
