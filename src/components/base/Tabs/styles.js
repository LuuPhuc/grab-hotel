import { makeStyles } from '@material-ui/core/styles'
import { colors } from '../../../assets/colors'
import { fonts } from '../../../assets/fonts'

export const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    '& .MuiAppBar-colorPrimary': {
      backgroundColor: colors.transparent
    },
    '& .MuiToolbar-regular': {
      minHeight: 120,
      margin: '0 auto'
    },
    '& .MuiAppBar-root': {
      width: '30%'
    },
    '& .MuiAppBar-positionAbsolute': {
      top: '126px',
      left: '206px',
      position: 'absolute'
    },
    '& .MuiBox-root': {
      backgroundColor: colors.gray
    }
  },
  paper: {
    position: 'absolute'
  },
  tabs: {
    paddingTop: '20px'
  },
  tab: {
    color: colors.black,
    fontWeight: fonts.bold
  },
  active_tab: {
    color: colors.black,
    fontWeight: fonts.bold,
    background: colors.gray,
    borderRadius: '15px 15px 0 0'
  },
  TheBestTravelApp: {
    width: 444,
    height: 63,
    fontFamily: 'HelveticaNeue',
    fontSize: fonts.h1,
    fontWeight: fonts.bold,
    fontStretch: 'normal',
    fontStyle: 'normal',
    lineHeight: 1.69,
    letterSpacing: '-0.22px',
    color: colors.black,
    top: 126,
    left: 70,
    textAlign: 'left'
  }
}))
