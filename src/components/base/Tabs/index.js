import React from 'react'
import PropTypes from 'prop-types'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
import TextFields from '../../base/TextFields'
import { useStyles } from './styles'

function TabPanel (props) {
  const { children, value, index, ...other } = props

  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  )
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
}

function a11yProps (index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`
  }
}

export default function SimpleTabs () {
  const classes = useStyles()
  const [value, setValue] = React.useState(0)

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  return (
    <div className={classes.root}>
      <AppBar position='absolute' elevation={0}>
        <Typography className={classes.TheBestTravelApp}>The Best Travel App</Typography>
        <Tabs value={value} onChange={handleChange} aria-label='simple tabs example' indicatorColor='transparent' className={classes.tabs}>
          <Tab label='Nghỉ Giờ' {...a11yProps(0)} className={value === 0 ? classes.active_tab : classes.tab} disableTouchRipple='true' />
          <Tab label='Qua Đêm' {...a11yProps(1)} className={value === 1 ? classes.active_tab : classes.tab} disableTouchRipple='true' />
          <Tab label='Nhiều Ngày' {...a11yProps(2)} className={value === 2 ? classes.active_tab : classes.tab} disableTouchRipple='true' />
        </Tabs>
        <TabPanel value={value} index={0}>
          <TextFields value={value} index={0} />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <TextFields value={value} index={0} />
        </TabPanel>
        <TabPanel value={value} index={2}>
          <TextFields value={value} index={0} />
        </TabPanel>
      </AppBar>
    </div>
  )
}
