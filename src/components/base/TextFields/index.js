import React from 'react'
import TextField from '@material-ui/core/TextField'
import { useStyles } from './styles'

export default function TextFields () {
  const classes = useStyles()

  return (
    <div>
      <TextField
        id='outlined-full-width'
        label='Label'
        style={{ margin: 8 }}
        placeholder='Placeholder'
        helperText='Full width!'
        fullWidth
        margin='normal'
        InputLabelProps={{
          shrink: true
        }}
        variant='outlined'
      />
    </div>
  )
}
