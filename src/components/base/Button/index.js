import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import { useStyles } from './styles'

function NewButton(props) {
    const classes = useStyles()
    return (
        // <Button className={classes[props.type || 'default']} >{props.label}</Button>
        <Button className={classes[props.type]} >{props.label}</Button>
    )
}

NewButton.defaultProps = {
    type: 'default',
    label: 'Button Text'
};
export default NewButton