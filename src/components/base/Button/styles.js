import { makeStyles } from '@material-ui/core/styles'
import withStyles from '@material-ui/core/styles/withStyles'
import { colors } from '../../../assets/colors'
import { fonts } from '../../../assets/fonts'
import { green } from '@material-ui/core/colors'
import { Colors, Fonts } from '../../../assets'



export const useStyles = makeStyles((theme) => ({
    default: {
        backgroundColor: colors.white
    },
    orange: {
        textTransform: 'none',
        fontWeight: fonts.bold,
        color: colors.white,
        backgroundColor: colors.orange,
        padding: '10px 40px',
        borderRadius: '30px',
        marginLeft: theme.spacing(2)
    },
    green: {
        textTransform: 'none',
        fontWeight: fonts.bold,
        color: colors.orange,
        backgroundColor: colors.green,
        padding: '10px 40px',
        borderRadius: '30px',
        marginLeft: theme.spacing(2)
    }
}))
