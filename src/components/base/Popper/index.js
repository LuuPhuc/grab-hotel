import React from 'react'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import { useStyles } from './styles'
import NewButton from '../Button'
import OrangeButton from '../../partials/OrangeButton'

export default function PositionedPopper() {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <Grid container justify='center' className={classes.grid}>
        <Grid item>
          <Button className={classes.button}>Home</Button>
          <Button className={classes.button}>Liên hệ</Button>
          <Button className={classes.button}>Đăng ký</Button>
          <NewButton>Dang Nhap</NewButton>
        </Grid>
      </Grid>
    </div>
  )
}
