import { makeStyles } from '@material-ui/core/styles'
import { colors } from '../../../assets/colors'
import { fonts } from '../../../assets/fonts'
export const useStyles = makeStyles((theme) => ({
  typography: {
    padding: theme.spacing(3)
  },
  button: {
    textTransform: 'none',
    fontWeight: fonts.bold
  },
  loginButton: {
    textTransform: 'none',
    fontWeight: fonts.bold,
    color: colors.white,
    backgroundColor: colors.orange,
    padding: '10px 40px',
    borderRadius: '30px',
    marginLeft: theme.spacing(2)
  },
  grid: {
    '& .MuiButton-text': {
      padding: '10px 35px'
    }
  }
}))
